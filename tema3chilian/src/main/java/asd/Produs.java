package asd;

public class Produs {

	private int id;
	private String nume;
	private String nume_producator;
	private double pret;

	public Produs(int id, String nume, String nume_producator, double pret) {
		this.id = id;
		this.nume = nume;
		this.nume_producator = nume_producator;
		this.pret = pret;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getNume_producator() {
		return nume_producator;
	}

	public void setNume_producator(String nume_producator) {
		this.nume_producator = nume_producator;
	}

	public double getPret() {
		return pret;
	}

	public void setPret(double pret) {
		this.pret = pret;
	}

}
