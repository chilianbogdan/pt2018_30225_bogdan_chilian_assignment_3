package asd;

public class Stock {
	
	private int id_produs;
	private int nr;
	
	
	public Stock(int id_produs, int nr) {
		
		this.id_produs = id_produs;
		this.nr = nr;
	}

	public int getId_produs() {
		return id_produs;
	}

	public void setId_produs(int id_produs) {
		this.id_produs = id_produs;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}
}

