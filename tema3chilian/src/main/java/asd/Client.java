package asd;


public class Client {
	private int id_client;
	private String nume;
	private String prenume;
	private String adresa;
	private String email;
	private String telefon;
	

	public Client(int id_client, String nume, String prenume, String adresa, String email, String telefon) {
		this.id_client = id_client;
		this.nume = nume;
		this.prenume = prenume;
		this.adresa = adresa;
		this.email = email;
		this.telefon = telefon;
	}


	public Client() {
		// TODO Auto-generated constructor stub
	}


	public int getId_client() {
		return id_client;
	}


	public void setId_client(int id_client) {
		this.id_client = id_client;
	}


	public String getNume() {
		return nume;
	}


	public void setNume(String nume) {
		this.nume = nume;
	}


	public String getPrenume() {
		return prenume;
	}


	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}


	public String getAdresa() {
		return adresa;
	}


	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefon() {
		return telefon;
	}


	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

}