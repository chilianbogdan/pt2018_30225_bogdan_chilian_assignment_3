package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintStream;



import javax.swing.ButtonGroup;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame{

	private static final long serialVersionUID = -8531830402858307850L;

	private JPanel inputPanel = new JPanel();
	private JPanel operationsPanel = new JPanel();
	private JPanel inputProductPanel = new JPanel();
	private JPanel inputCustomerPanel = new JPanel();
	private JPanel backPanel = new JPanel();
	private JPanel consolePanel = new JPanel();

	private JDialog deleteNodeDialog;
	private JDialog findNodeDialog;
	private JDialog traverseTreeDialog;
	private JDialog itemQuantityDialog;
	private JDialog findIdDialog;
	private JDialog checkUnderstockDialog;
	private JDialog checkOverstockDialog;
	private JDialog chooseDialog = new JDialog();
	private JDialog nodeComparisonDialog;

	private JLabel customerNameLabel  = new JLabel("Customer Name");
	private JLabel customerNameLabel2  = new JLabel("Customer Name");
	private JLabel customerAddressLabel  = new JLabel("Customer address");
	private JLabel customerPhoneLabel  = new JLabel("Customer Phone");
	private JLabel productNameLabel = new JLabel ("Product Name");
	private JLabel productNameLabel2 = new JLabel ("Product Name");
	private JLabel productNameLabela = new JLabel ("Product A Name");
	private JLabel productNameLabelb = new JLabel ("Product B Name");
	private JLabel productQuantityLabel = new JLabel ("Product Quantity");
	private JLabel productQuantityLabel2 = new JLabel ("Product Quantity");
	private JLabel productPriceLabel = new JLabel ("Product price");
	private JLabel productIdLabel = new JLabel ("Product id");
	private JLabel nodeNumberLabel = new JLabel("Enter node number");
	private JLabel traverseModeLabel = new JLabel("Enter traverse mode (lowercase)");
	private JLabel maximumValueLabel = new JLabel ("Maximum value");
	private JLabel minimumValueLabel = new JLabel ("Minimum value");

	private JTextField customerNameField = new JTextField ("cust");
	private JTextField customerAddressField = new JTextField ("zanzibar");
	private JTextField customerPhoneField = new JTextField ("1233");
	private JTextField productNameField = new JTextField ("piston");
	private JTextField productNameField2 = new JTextField ("piston");
	private JTextField productNameField3 = new JTextField ("piston");
	private JTextField productNameField4 = new JTextField ("piston");
	private JTextField productNameField5 = new JTextField ("piston");
	private JTextField productNameFielda = new JTextField ("piston");
	private JTextField productNameFieldb = new JTextField ("piston");
	private JTextField productIdField = new JTextField ("1");
	private JTextField productQuantityField = new JTextField ("3");
	private JTextField productQuantityField2 = new JTextField ("3");
	private JTextField productPriceField = new JTextField ("30");
	private JTextField nodeNumber = new JTextField();
	private JTextField minimumValueField = new JTextField();
	private JTextField maximumValueField = new JTextField();

	private JButton customer = new JButton ("Customer");
	private JButton admin = new JButton ("Administrator");
	private JButton addOrderButton = new JButton ("Add order");
	private JButton addCustomerButton = new JButton ("Add customer");
	private JButton showCustomersButton = new JButton ("Show customers");
	private JButton addProductButton = new JButton ("Add product");
	private JButton deleteOk = new JButton("Delete");
	private JButton traverseOk = new JButton("Traverse");
	private JButton findOk = new JButton("Find");
	private JButton checkOverstockOk = new JButton ("Check overstock");
	private JButton checkUnderstockOk = new JButton ("Check understock");
	private JButton itemQuantityOk = new JButton ("Quantity");
	private JButton findId = new JButton ("Find id");
	private JButton nodeComparison = new JButton ("Compare");

	private JRadioButton radioOrder = new JRadioButton ("Order tree");
	private JRadioButton radioProduct = new JRadioButton ("Product tree");
	private JComboBox<?> opCombo; //combo box to hold the available tree operations
	private JComboBox<String> customerCombo; //combo box to hold the customers in the OPDept

	private boolean okOrder=true; //this boolean is used to check if we operate on the order tree or on the product tree
	private boolean saved=true; //boolean to check if the current state of the application has been modified and not saved
	//private Company company;
	JTextArea ta ;
	//TextAreaOutputStream taos ;
	PrintStream ps ;
	GUI(){

		//company=new Company();
		customerCombo = new JComboBox<String>();
		String[] opStrings = {"Select an operation", "Show",  "Find","Find id", "Delete","Traverse","Save","Quantity of an item","Total quantity","Check overstock","Check understock","Node comparison"};
		opCombo = new JComboBox<Object>(opStrings);
		opCombo.setSelectedIndex(0);
		chooseDialog.setLayout(new GridLayout(10,2)); //this is the first window and it contains 2 buttons; one for client and one for admin
		chooseDialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		chooseDialog.setSize(400,400);
		chooseDialog.add(customer);
		chooseDialog.add(admin);
		chooseDialog.setVisible(true);
		ta= new JTextArea();
		try {
			//taos= new TextAreaOutputStream( ta, 60 );
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	//	ps= new PrintStream( taos );
		System.setOut( ps );
		System.setErr( ps );


		
		//restore the trees and opdept if the files exist and are not null
		try{
		//	if(company.restoreOrderTree()) company.restoreOrderTree();
		//	if (company.restoreProductTree()) company.restoreProductTree();
		//	if (company.restoreOPDeptAL()) company.restoreOPDeptAL();

		//	for (int i=0;i<company.getCustomers().length;i++)
		//		customerCombo.addItem(company.getCustomers()[i]);
		}catch (Exception e){
			JOptionPane.showMessageDialog(null,"Cannot restore!");
		}
		addListeners();
		addWindowListener();

	}
	/**
	 * Method to add the action when the user closes the window from [X]
	 */
	private void addWindowListener(){
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e)
			{
				if(!saved)  displayConfirmExitDialog();
				else System.exit(0);
			}
		});
	}
	/**
	 * This method checks the fields required to input a new order
	 * These fields are available to both admin and customer
	 * If the fields are empty, a dialog is displayed
	 * else, a new order is made
	 */
	private void checkOrderFields(){
		String customerName = null;

		if (customerCombo.getSelectedIndex()==-1){
			JOptionPane.showMessageDialog(null,"Please choose your name");
		}
		else customerName=(String)customerCombo.getSelectedItem();

		String productName = null;
		if (productNameField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the product name");
		}
		else productName=productNameField.getText();


		String productQuantity = null;
		if (productQuantityField2.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the quantity");
		}
		else productQuantity=productQuantityField.getText();

		//if (!customerName.isEmpty() && !productName.isEmpty())
			//company.addOrder(customerName,productName, company.findId(productName), Integer.parseInt(productQuantity));

	}
	/**
	 * This method checks the fields required to input a new product
	 * These fields are available only to the admin
	 * If the fields are empty, a dialog is displayed
	 * else, a new product is made
	 */
	private void checkProductFields(){

		String productName = null;
		if (productNameField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the product name");
		}
		else productName=productNameField.getText();


		int productPrice = 0;
		if (productPriceField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the price");
		}
		else productPrice=Integer.parseInt(productPriceField.getText());

		int productId = 0;
		if (productIdField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the id");
		}
		else productId=Integer.parseInt(productIdField.getText());

		int qty=0;
		if (productQuantityField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the quantity");
		}
		else qty=Integer.parseInt(productQuantityField.getText());
		//if (!productName.isEmpty())
		//	company.addProduct(productName, productId, productPrice,qty);

	}
	/**
	 * This method checks the fields required to input a new customer
	 * These fields are available to both admin and customer
	 * If the fields are empty, a dialog is displayed
	 * else, a new customer is inserted
	 */
	private void checkCustomerFields(){

		String customerName = null;
		if (customerNameField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the client name");
		}
		else customerName=customerNameField.getText();


		String customerAddress = null;
		if (customerAddressField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the address");
		}
		else customerAddress=customerAddressField.getText();

		String customerPhone= null;
		if (customerPhoneField.getText().isEmpty()){
			JOptionPane.showMessageDialog(null,"Please enter the phone number");
		}
		else customerPhone=customerPhoneField.getText();
	//	if (!customerName.isEmpty())
		//	company.addCustomer(customerName, customerPhone, customerAddress);

	}
	/**
	 * This method creates the panels available to the customer
	 */

	private void addCustomerCommands(){
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize (500,500);
		setContentPane(backPanel);
		inputPanel.setLayout(new GridLayout(5,2));
		inputPanel.add(customerNameLabel2);
		inputPanel.add(customerCombo);

		inputPanel.add(productNameLabel2);
		inputPanel.add(productNameField2);
		inputPanel.add(productQuantityLabel2);
		inputPanel.add(productQuantityField2);
		inputPanel.add(addOrderButton);
		inputPanel.add(showCustomersButton);

		inputCustomerPanel.setLayout(new GridLayout(5,2));
		inputCustomerPanel.add(customerNameLabel);
		inputCustomerPanel.add(customerNameField);
		inputCustomerPanel.add(customerAddressLabel);
		inputCustomerPanel.add(customerAddressField);
		inputCustomerPanel.add(customerPhoneLabel);
		inputCustomerPanel.add(customerPhoneField);
		inputCustomerPanel.add(addCustomerButton);
		inputCustomerPanel.add(showCustomersButton);

		backPanel.setLayout (new GridLayout(4,1));
		backPanel.add(inputPanel);
		backPanel.add(inputCustomerPanel);
		setVisible(true);
	}
	/** 
	 * This method creates the panel available to the admin
	 */
	private void addAdminCommands(){

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize (500,500);
		setContentPane(backPanel);


		operationsPanel.setLayout(new GridLayout (5,3));

		ButtonGroup group = new ButtonGroup();
		group.add(radioOrder);
		group.add(radioProduct);
		radioOrder.setSelected(true);
		operationsPanel.add(radioOrder);
		operationsPanel.add(radioProduct);
		operationsPanel.add(opCombo);


		inputProductPanel.setLayout(new GridLayout(5,2));
		inputProductPanel.add(productNameLabel);
		inputProductPanel.add(productNameField);
		inputProductPanel.add(productPriceLabel);		
		inputProductPanel.add(productPriceField);
		inputProductPanel.add(productIdLabel);
		inputProductPanel.add(productIdField);
		inputProductPanel.add(productQuantityLabel);
		inputProductPanel.add(productQuantityField);
		inputProductPanel.add(addProductButton);
		
		inputCustomerPanel.setLayout(new GridLayout(5,2));
		inputCustomerPanel.add(customerNameLabel);
		inputCustomerPanel.add(customerNameField);
		inputCustomerPanel.add(customerAddressLabel);
		inputCustomerPanel.add(customerAddressField);
		inputCustomerPanel.add(customerPhoneLabel);
		inputCustomerPanel.add(customerPhoneField);
		inputCustomerPanel.add(addCustomerButton);
		inputCustomerPanel.add(showCustomersButton);
		
		consolePanel.setLayout(new GridLayout(1,1));
		consolePanel.add( new JScrollPane( ta )  );
		
		backPanel.setLayout (new GridLayout(4,1));
		backPanel.add(operationsPanel);
		backPanel.add(inputCustomerPanel);
		backPanel.add(inputProductPanel);	
		backPanel.add(consolePanel);

		setVisible(true);
	}
	private void saveModifications(){
		//company.saveOrderTree();
		//company.saveProductTree();
		//company.saveOPDeptAL();
		saved=true;
	}
	private void displayConfirmExitDialog(){
		int reply =JOptionPane.showConfirmDialog(null, "Do you want to save the modifications ?","Confirm exit",JOptionPane.YES_NO_OPTION);
		if (reply == JOptionPane.NO_OPTION)
		{
			System.exit(0);
		}
		if (reply == JOptionPane.YES_OPTION)
		{
			saveModifications();
			System.exit(0);
		}
	}
	/**
	 * Method to add listeners to the operations combo box and the buttons
	 */
	private void addListeners(){

		
		class ComboListener implements ItemListener{

			public void itemStateChanged(ItemEvent evt) {


				// Get the affected item
				Object item = evt.getItem();

				if (evt.getStateChange() == ItemEvent.SELECTED) {
					// Item was just selected

					if (item.toString().equals("Show")){

						if (okOrder){
						//	company.displayOrderTree();

						}
						if (!okOrder){
						//	company.displayProductTree();

						}
					}
					else if (item.toString().equals( "Find")){

						findNodeDialog = new JDialog();
						findNodeDialog.setSize(200,200);

						findNodeDialog.setLayout(new GridLayout(5,2));
						findNodeDialog.add(nodeNumberLabel);
						findNodeDialog.add(nodeNumber);
						findNodeDialog.add(findOk);
						findNodeDialog.setVisible(true);

					}
					else if (item.toString().equals( "Delete")){


						deleteNodeDialog = new JDialog();
						deleteNodeDialog.setSize(200,200);
						deleteNodeDialog.setLayout(new GridLayout(5,2));
						deleteNodeDialog.add(nodeNumberLabel);
						deleteNodeDialog.add(nodeNumber);
						deleteNodeDialog.add(deleteOk);
						deleteNodeDialog.setVisible(true);



					}

					else if (item.toString().equals( "Traverse")){

						traverseTreeDialog = new JDialog();
						traverseTreeDialog.setSize(200,200);
						traverseTreeDialog.setLayout(new GridLayout(5,2));
						traverseTreeDialog.add(traverseModeLabel);
						traverseTreeDialog.add(nodeNumber);
						traverseTreeDialog.add(traverseOk);
						traverseTreeDialog.setVisible(true);

					}

					else if (item.toString().equals( "Save")){
						if (!saved) saveModifications();
					}

					else if (item.toString().equals( "Quantity of an item")){
						if (!okOrder){	itemQuantityDialog = new JDialog();
						itemQuantityDialog.setSize(200,200);
						itemQuantityDialog.setLayout(new GridLayout(5,2));
						itemQuantityDialog.add(productNameLabel);
						itemQuantityDialog.add(productNameField3);
						itemQuantityDialog.add(itemQuantityOk);
						itemQuantityDialog.setVisible(true);
						}
					}
					else if (item.toString().equals("Total quantity")){
						if (!okOrder){
							//System.out.println ("Total quantity of all items is: "+company.totalQuantity());
						}
					}
					else if (item.toString().equals("Check overstock")){
						if (!okOrder){
							checkOverstockDialog = new JDialog();
							checkOverstockDialog.setSize(200,200);
							checkOverstockDialog.setLayout(new GridLayout(5,2));
							checkOverstockDialog.add(productNameLabel);
							checkOverstockDialog.add(productNameField4);
							checkOverstockDialog.add(maximumValueLabel);
							checkOverstockDialog.add(maximumValueField);
							checkOverstockDialog.add(checkOverstockOk);
							checkOverstockDialog.setVisible(true);
						}
					}
					else if (item.toString().equals("Check understock")){
						if (!okOrder){
							checkUnderstockDialog = new JDialog();
							checkUnderstockDialog.setSize(200,200);
							checkUnderstockDialog.setLayout(new GridLayout(5,2));
							checkUnderstockDialog.add(productNameLabel);
							checkUnderstockDialog.add(productNameField5);
							checkUnderstockDialog.add(minimumValueLabel);
							checkUnderstockDialog.add(minimumValueField);
							checkUnderstockDialog.add(checkUnderstockOk);
							checkUnderstockDialog.setVisible(true);
						}
					}
					else if (item.toString().equals("Find id")){
						findIdDialog = new JDialog();
						findIdDialog.setSize(200,200);

						findIdDialog.setLayout(new GridLayout(5,2));
						findIdDialog.add(productNameLabel);
						findIdDialog.add(productNameField5);
						findIdDialog.add(findId);
						findIdDialog.setVisible(true);
					}
					else if (item.toString().equals("Node comparison")){
						nodeComparisonDialog = new JDialog();
						nodeComparisonDialog.setSize(200,200);

						nodeComparisonDialog.setLayout(new GridLayout(5,2));
						nodeComparisonDialog.add(productNameLabela);
						nodeComparisonDialog.add(productNameFielda);
						nodeComparisonDialog.add(productNameLabelb);
						nodeComparisonDialog.add(productNameFieldb);
						nodeComparisonDialog.add(nodeComparison);
						nodeComparisonDialog.setVisible(true);
					}

				} else if (evt.getStateChange() == ItemEvent.DESELECTED) {
					// Item is no longer selected
				}
			}


		}
		
	}


}
