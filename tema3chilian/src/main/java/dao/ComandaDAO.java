package dao;


import connection.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import asd.Comanda;
import asd.Produs;


public class ComandaDAO {
    public void insert(Comanda comanda)
    {
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        StockDAO st=new StockDAO();
        try
        {
            ProdusDAO pdao=new ProdusDAO();
            try {
                List<Produs> produse=pdao.selectAll();
                for (Produs produs:produse) {
                	
                    if(comanda.getId_produs()==st.findById(produs.getId()).getId_produs()) {
                        if (comanda.getCantitate() <= st.findById(produs.getId()).getNr()) {
                            int nval;
                            nval =st.findById(produs.getId()).getNr() - comanda.getCantitate();
                            st.findById(produs.getId()).setNr(nval);

                           /* int idpp=produs.getId();
                            produs.setId(idpp);

                            String numepp=produs.getNume();
                            produs.setNume(numepp);

                            String numepr=produs.getNume_producator();
                            produs.setNume_producator(numepr);
                            
                            Double pretpret=produs.getPret();
                            produs.setPret(pretpret);*/

                            pdao.update(produs,produs.getId());
                        } else{
                            System.out.println("nu s-a putut efectua comanda");
                            return;
                        }

                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            connection=ConnectionFactory.getConnection();
            preparedStatement=connection.prepareStatement("INSERT INTO comanda (id_comanda,id_produs,id_client,cantitate)"
                    + " VALUES (?,?,?,?)");
            preparedStatement.setInt(1,comanda.getId_comanda());
            preparedStatement.setInt(2,comanda.getId_produs());
            preparedStatement.setInt(3,comanda.getId_client());
            preparedStatement.setInt(4,comanda.getCantitate());

            preparedStatement.executeUpdate();
            System.out.println("INSERT INTO comanda");
            
        }catch(Exception e)
        {
            e.printStackTrace();
        }

        finally{
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }

        try {
            //Factura.metoda(comanda);
        } catch (Exception e) {
            e.printStackTrace();
        }
       

    }

    public List<Comanda> selectAll() throws SQLException {
        List<Comanda> comenzi=new ArrayList<Comanda>();
        Connection connection=null;
        Statement statement =null;
        ResultSet rs=null;
        try
        {
            connection=ConnectionFactory.getConnection();
            statement=connection.createStatement();
            rs=statement.executeQuery("SELECT * FROM comanda");
            while(rs.next())
            {
                Comanda comanda =new Comanda(0, 0, 0, 0);
                comanda.setId_comanda(rs.getInt("id_comanda"));
                comanda.setId_produs(rs.getInt("id_produs"));
                comanda.setId_client(rs.getInt("id_client"));
                comanda.setCantitate(rs.getInt("cantitate"));
                comenzi.add(comanda);

            }

        }catch(Exception e){
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
			ConnectionFactory.close(rs);
        }
        return comenzi;
    }
    public void delete (int id)
    {
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement preparedStatement=null;
        try
        {
            preparedStatement=connection.prepareStatement("DELETE FROM comanda WHERE id_comanda = ?");
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            System.out.println("stergere comanda");

        }catch(Exception e)
        {
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }

    }
    public void update (Comanda comanda,int id)
    {
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        try
        {
            ProdusDAO pdao=new ProdusDAO();
            StockDAO st=new StockDAO();
            try {
                List<Produs> produse=pdao.selectAll();
                for (Produs produs:produse) {
                    if(comanda.getId_produs()==produs.getId()) {
                        if (comanda.getCantitate() <= st.findById(produs.getId()).getNr()) {
                            int nval;
                            nval = st.findById(produs.getId()).getNr() - comanda.getCantitate();
                            st.findById(produs.getId()).setNr(nval);

                            int idpp=produs.getId();
                            produs.setId(idpp);

                            String numepp=produs.getNume();
                            produs.setNume(numepp);

                            String idprpr=produs.getNume_producator();
                            produs.setNume_producator(idprpr);
                            
                            double prpr=produs.getPret();
                            produs.setPret(prpr);
                            
                            pdao.update(produs,produs.getId());
                            //pdao.update(produs,produs.getIdprodus());
                        } else{
                            System.out.println("nu s-a putut efectua  update la comanda");
                            return;
                        }

                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }



            connection=ConnectionFactory.getConnection();
            preparedStatement=connection.prepareStatement("UPDATE comanda SET "+ "id_produs = ?,id_client = ?,cantitate = ? WHERE id_comanda = ?");


            preparedStatement.setInt(1,comanda.getId_produs());
            preparedStatement.setInt(2,comanda.getId_client());
            preparedStatement.setInt(3,comanda.getCantitate());
            preparedStatement.setInt(4,id);

            preparedStatement.executeUpdate();
            System.out.println("update comanda");

        }catch(Exception e)
        {
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }
    }

}

