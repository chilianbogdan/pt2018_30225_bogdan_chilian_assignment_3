package dao;

import connection.ConnectionFactory;
import asd.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.sql.Statement;

public class ClientDAO {


	public Client findById(int studentId) {
		Client toReturn = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM client WHERE id_client = ?");
			preparedStatement.setLong(1, studentId);
			rs = preparedStatement.executeQuery();
			rs.next();

			int id_client = rs.getInt("id_client");
			String nume = rs.getString("nume");
			String prenume = rs.getString("prenume");
			String adresa = rs.getString("adresa");
			String email = rs.getString("email");
			String telefon = rs.getString("telefon");
			toReturn = new Client(id_client, nume,prenume, adresa, email, telefon);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}

	public List<Client> selectAll() throws SQLException {
		List<Client> clienti = new ArrayList<Client>();
		Connection connection = ConnectionFactory.getConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM client");
		try {
			while (rs.next()) {
				Client client = new Client();
				client.setId_client(rs.getInt("id_client"));
				client.setNume(rs.getString("nume"));
				client.setPrenume(rs.getString("prenume"));
				client.setAdresa(rs.getString("adresa"));
				client.setEmail(rs.getString("email"));
				client.setTelefon(rs.getString("telefon"));
				clienti.add(client);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(connection);
		}
		return clienti;
	}

	public void delete(int id) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement("DELETE FROM client WHERE id_client = ?");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			System.out.println("stergere");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}

	}

	public void update(Client client, int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionFactory.getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE client SET " + "nume = ?,prenume = ?,adresa = ?,email= ?,telefon= ? WHERE id_client = ?");

			preparedStatement.setInt(6, id);
			preparedStatement.setString(1, client.getNume());
			preparedStatement.setString(2, client.getAdresa());
			preparedStatement.setString(3, client.getEmail());
			preparedStatement.setString(4, client.getAdresa());
			preparedStatement.setString(5, client.getEmail());
			preparedStatement.executeUpdate();

			System.out.println("update client");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}
	}

	public void insert(Client client) {
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = connection.prepareStatement(
					"INSERT INTO client (id_client,nume,prenume,adresa,email,telefon)" + " VALUES (?,?,?,?,?,?)");
			preparedStatement.setInt(1, client.getId_client());
			preparedStatement.setString(2, client.getNume());
			preparedStatement.setString(3, client.getPrenume());
			preparedStatement.setString(4, client.getAdresa());
			preparedStatement.setString(5, client.getEmail());
			preparedStatement.setString(6, client.getTelefon());
			preparedStatement.executeUpdate();
			System.out.println(
					"INSERT INTO client(id_client,nume,prenume,adresa,email,telefon)" + "VALUES (?,?,?,?,?,?)");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}

	}
	
	
	

}