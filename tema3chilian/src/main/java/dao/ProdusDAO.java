package dao;


import connection.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import asd.Produs;



public class ProdusDAO {
	
	
	public Produs findById(int id_pr) {
		Produs toReturn = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM produs WHERE id = ?");
			preparedStatement.setLong(1, id_pr);
			rs = preparedStatement.executeQuery();
			rs.next();

			int id = rs.getInt("id");
			String nume = rs.getString("nume");
			String nume_producator = rs.getString("nume_producator");
			double pret = rs.getDouble("pret");
			
			toReturn = new Produs(id, nume,nume_producator,pret);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	
    public void insert(Produs produs)
    {
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement preparedStatement=null;
        try
        {
            preparedStatement=connection.prepareStatement("INSERT INTO produs (id,nume,nume_producator,pret)"
                    + " VALUES (?,?,?,?)");
            preparedStatement.setInt(1,produs.getId());
            preparedStatement.setString(2,produs.getNume());
            preparedStatement.setString(3,produs.getNume_producator());
            preparedStatement.setDouble(4,produs.getPret());

            preparedStatement.executeUpdate();
            System.out.println("INSERT INTO produs");
        }catch(Exception e)
        {
            e.printStackTrace();

        }finally{
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }

    }

    public List<Produs> selectAll() throws SQLException {
        List<Produs> produse=new ArrayList<Produs>();
        Connection connection=ConnectionFactory.getConnection();
        Statement statement =null;
        ResultSet rs=null;
        try
        {
            statement=connection.createStatement();
            rs=statement.executeQuery("SELECT * FROM produs");
            while(rs.next())
            {
                Produs produs =new Produs(0,null,null,0);
                produs.setId(rs.getInt("id"));
                produs.setNume(rs.getString("nume"));
                produs.setNume_producator(rs.getString("nume_producator"));
                produs.setPret(rs.getDouble("pret"));
                produse.add(produs);

            }

        }catch(Exception e){
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(rs);
			ConnectionFactory.close(connection);
        }
        return produse;
    }
    public void delete (int id)
    {
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement preparedStatement=null;
        try
        {
            preparedStatement=connection.prepareStatement("DELETE FROM produs WHERE id = ?");
            preparedStatement.setInt(1,id);
            preparedStatement.executeUpdate();
            System.out.println("stergere produs");

        }catch(Exception e)
        {
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }

    }
    public void update (Produs produs,int id)
    {
        Connection connection=ConnectionFactory.getConnection();
        PreparedStatement preparedStatement=null;
        try
        {
            preparedStatement=connection.prepareStatement("UPDATE produs SET "+ "nume = ?,nume_producator = ?,pret = ? WHERE id = ?");
            preparedStatement.setString(1,produs.getNume());
            preparedStatement.setString(2,produs.getNume_producator());
            preparedStatement.setDouble(3,produs.getPret());
            preparedStatement.setInt(4,id);

            preparedStatement.executeUpdate();
            System.out.println("update produs");

        }catch(Exception e)
        {
            e.printStackTrace();
        }finally
        {
        	ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
        }
    }
}
