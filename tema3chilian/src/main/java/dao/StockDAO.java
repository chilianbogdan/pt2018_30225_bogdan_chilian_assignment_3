package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import asd.Stock;
import connection.ConnectionFactory;

public class StockDAO {
	
	
	public Stock findById(int id_p) {
		Stock toReturn = null;
		Connection connection = ConnectionFactory.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			preparedStatement = connection.prepareStatement("SELECT * FROM stock WHERE id_produs = ?");
			preparedStatement.setLong(1, id_p);
			rs = preparedStatement.executeQuery();
			rs.next();

			int id = rs.getInt("id_produs");
			int nr = rs.getInt("nr");
			toReturn = new Stock(id,nr);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(preparedStatement);
			ConnectionFactory.close(connection);
		}
		return toReturn;
	}
	
	 public List<Stock> selectAll() throws SQLException {
	        List<Stock> stockuri=new ArrayList<Stock>();
	        Connection connection=ConnectionFactory.getConnection();
	        Statement statement =null;
	        ResultSet rs=null;
	        try
	        {
	            statement=connection.createStatement();
	            rs=statement.executeQuery("SELECT * FROM stock");
	            while(rs.next())
	            {
	                Stock stock =new Stock(0,0);
	                stock.setId_produs(rs.getInt("id_produs"));
	                stock.setNr(rs.getInt("nr"));
	                stockuri.add(stock);

	            }

	        }catch(Exception e){
	            e.printStackTrace();
	        }finally
	        {
	        	ConnectionFactory.close(rs);
				ConnectionFactory.close(connection);
	        }
	        return stockuri;
	    }



}
